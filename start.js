require('dotenv').config();
const puppeteer = require('puppeteer');


/** open the browser and start in full screen app mode. See Chromium Command Line switches
  - https://peter.sh/experiments/chromium-command-line-switches/


  Set local policy for Chromium
  - Linux: https://www.chromium.org/administrators/linux-quick-start
  - Mac: copy org.chromium.Chromium.plist to /Library/Preferences (requires root)

**/
(async () => {
  const browser = await puppeteer.launch({
  	headless: false,
  	// args: ['--start-fullscreen', '--no-startup-window'],
  	// args: ['--start-fullscreen', '--kiosk', '--disable-infobars'],
  	args: ['--start-fullscreen', '--kiosk', '--disable-infobars', '--disable-save-password-bubble'],
  	//appMode: true,
  });
  const page = await browser.newPage();
  await page.setViewport({
    width: 1920,
    height: 1080,
  });

  await page.goto(process.env.DASHBOARD_PAGE);

  // let's login
  const elementHandle = await page.$('input[type=email]');
  await elementHandle.type( process.env.DASHBOARD_USER );
  await page.click("#identifierNext");
  await page.waitForNavigation({ timeout: 3000, waitUntil: 'networkidle0'});
  
  const pw = await page.$('input[type=password]');
  await pw.type(process.env.DASHBOARD_PASS);
  await page.click("div#passwordNext");

})();